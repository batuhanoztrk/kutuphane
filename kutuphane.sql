-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 03 Ara 2017, 22:39:15
-- Sunucu sürümü: 10.1.28-MariaDB
-- PHP Sürümü: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `palkutuphane`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `guven`
--

CREATE TABLE `guven` (
  `guven_id` int(11) NOT NULL,
  `ogrenci_id` int(11) NOT NULL,
  `aldigi_kitaps` int(11) NOT NULL DEFAULT '0',
  `zamaninda_teslims` int(11) NOT NULL DEFAULT '0',
  `gec_teslims` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `idare`
--

CREATE TABLE `idare` (
  `idare_id` int(11) NOT NULL,
  `kadi` varchar(255) NOT NULL,
  `sifre` varchar(255) NOT NULL,
  `eposta` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `idare` (`idare_id`, `kadi`, `sifre`, `eposta`) VALUES
(1, 'kutuphane', '693cfed9dd8adf7c63afbf53cf3a8043', 'kutuphane@localhost');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kitaplar`
--

CREATE TABLE `kitaplar` (
  `kitaplar_id` int(11) NOT NULL,
  `kitap_adi` varchar(255) NOT NULL,
  `kitap_yazar` varchar(255) NOT NULL,
  `kitap_konu` varchar(255) NULL,
  `kitap_tur` int(11) DEFAULT NULL,
  `isbn` bigint(20) NOT NULL,
  `tasnif` bigint(20) NOT NULL,
  `sira` varchar(255) NOT NULL,
  `okunma` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kutuphane`
--

CREATE TABLE `kutuphane` (
  `id` int(11) NOT NULL,
  `ogrenci_id` int(11) NOT NULL,
  `kitap_id` int(11) NOT NULL,
  `alim_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tahmini_teslim_tarihi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `teslim_tarihi` date DEFAULT NULL,
  `teslim_edildimi` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ogrenciler`
--

CREATE TABLE `ogrenciler` (
  `ogrenciler_id` int(11) NOT NULL,
  `ad_soyad` varchar(255) NOT NULL,
  `no` int(11) NOT NULL,
  `okod` longtext NOT NULL,
  `kitap_varmi` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `turler`
--

CREATE TABLE `turler` (
  `tur_id` int(11) NOT NULL,
  `tur_adi` varchar(255) NOT NULL,
  `kitaps` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `guven`
--
ALTER TABLE `guven`
  ADD PRIMARY KEY (`guven_id`),
  ADD KEY `ogrenci_id` (`ogrenci_id`);

--
-- Tablo için indeksler `idare`
--
ALTER TABLE `idare`
  ADD PRIMARY KEY (`idare_id`);

--
-- Tablo için indeksler `kitaplar`
--
ALTER TABLE `kitaplar`
  ADD PRIMARY KEY (`kitaplar_id`),
  ADD KEY `kitap_tur` (`kitap_tur`);

--
-- Tablo için indeksler `kutuphane`
--
ALTER TABLE `kutuphane`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ogrenci-kitap` (`ogrenci_id`,`kitap_id`),
  ADD KEY `kitap` (`kitap_id`);

--
-- Tablo için indeksler `ogrenciler`
--
ALTER TABLE `ogrenciler`
  ADD PRIMARY KEY (`ogrenciler_id`);

--
-- Tablo için indeksler `turler`
--
ALTER TABLE `turler`
  ADD PRIMARY KEY (`tur_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `guven`
--
ALTER TABLE `guven`
  MODIFY `guven_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `idare`
--
ALTER TABLE `idare`
  MODIFY `idare_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `kitaplar`
--
ALTER TABLE `kitaplar`
  MODIFY `kitaplar_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `kutuphane`
--
ALTER TABLE `kutuphane`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `ogrenciler`
--
ALTER TABLE `ogrenciler`
  MODIFY `ogrenciler_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `turler`
--
ALTER TABLE `turler`
  MODIFY `tur_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `guven`
--
ALTER TABLE `guven`
  ADD CONSTRAINT `guven_ibfk_1` FOREIGN KEY (`ogrenci_id`) REFERENCES `ogrenciler` (`ogrenciler_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `kitaplar`
--
ALTER TABLE `kitaplar`
  ADD CONSTRAINT `kitaplar_ibfk_1` FOREIGN KEY (`kitap_tur`) REFERENCES `turler` (`tur_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `kutuphane`
--
ALTER TABLE `kutuphane`
  ADD CONSTRAINT `kitap` FOREIGN KEY (`kitap_id`) REFERENCES `kitaplar` (`kitaplar_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ogrenci` FOREIGN KEY (`ogrenci_id`) REFERENCES `ogrenciler` (`ogrenciler_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
