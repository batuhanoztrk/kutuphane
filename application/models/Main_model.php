<?php

/**
 * Created by PhpStorm.
 * User: Batuhan
 * Date: 31.03.2017
 * Time: 17:14
 */
class Main_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function eco()
    {
        $eco = $this->db->query("SELECT ad_soyad,aldigi_kitaps,ogrenciler_id FROM guven INNER JOIN ogrenciler ON guven.ogrenci_id = ogrenciler .ogrenciler_id WHERE aldigi_kitaps = (SELECT MAX(aldigi_kitaps) FROM guven) and aldigi_kitaps != 0 ORDER BY rand() LIMIT 1");
        if ($this->db->affected_rows() >= 1) {
            return $eco->row_array();
        }
    }

    public function eck()
    {
        $eck = $this->db->query("SELECT kitap_id, COUNT(*) AS sayi FROM kutuphane GROUP BY kitap_id ORDER BY sayi DESC LIMIT 1")->row_array();
        $eck_kad = $this->db->where("kitaplar_id", $eck['kitap_id'])->get("kitaplar");
        if ($this->db->affected_rows() >= 1) {
            return $eck_kad->row_array();
        }
    }

    public function eao()
    {
        $eao = $this->db->query("SELECT ad_soyad,aldigi_kitaps,ogrenciler_id FROM guven INNER JOIN ogrenciler ON guven.ogrenci_id = ogrenciler.ogrenciler_id WHERE aldigi_kitaps < (SELECT ROUND(AVG(aldigi_kitaps)) FROM guven) and aldigi_kitaps != 0 ORDER BY rand() LIMIT 1");
        if ($this->db->affected_rows() >= 1) {
            return $eao->row_array();
        }
    }

    public function ho()
    {
        $ho = $this->db->query("SELECT ad_soyad,ogrenciler_id FROM guven INNER JOIN ogrenciler ON guven.ogrenci_id = ogrenciler.ogrenciler_id WHERE aldigi_kitaps = 0 ORDER BY rand() LIMIT 1");
        if ($this->db->affected_rows() >= 1) {
            return $ho->row_array();
        }
    }

    public function qrogrenci($qr)
    {
        $kontrol = $this->db->select('okod,ogrenciler_id,kitap_varmi')->where("okod", $qr)->get("ogrenciler");
        if ($this->db->affected_rows() >= 1) {
            $sonuc = $kontrol->row_array();
            if ($sonuc['kitap_varmi'] == 0) {
                $data['islem'] = "1";
                $data['sonuc'] = $sonuc;
            } else {
                $data['islem'] = "2";
                $data['sonuc'] = $sonuc;
            }
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function qrkitap($qr)
    {
        $kontrol = $this->db->select('isbn,kitaplar_id,okunma')->where("isbn", $qr)->where("okunma", 0)->limit(1)->get('kitaplar');
        if ($this->db->affected_rows() >= 1) {
            $sonuc = $kontrol->row_array();
            $data['islem'] = "1";
            $data['sonuc'] = $sonuc;
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function kutuphanekitap($oid)
    {
        $id = $this->security->xss_clean($oid);
        $kontrol = $this->db->where("ogrenci_id", $id)->order_by('alim_tarihi DESC, id DESC')->limit(1)->get('kutuphane');
        if ($this->db->affected_rows() >= 1) {
            $sonuc2 = $kontrol->row_array();
            $kitap = $this->db->select('isbn,kitaplar_id,okunma')->where('kitaplar_id', $sonuc2['kitap_id'])->get('kitaplar');
            $sonuc = $kitap->row_array();
            if ($sonuc['okunma'] == 0) {
                $data['islem'] = "1";
                $data['sonuc'] = $sonuc;
            } else {
                $data['islem'] = "2";
                $data['sonuc'] = $sonuc;
            }
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function kitapal($bilgiler)
    {
        $this->db->trans_start();
        $this->db->insert('kutuphane', $bilgiler);
        $this->db->where('ogrenciler_id', $bilgiler['ogrenci_id'])->set('kitap_varmi', "1")->update('ogrenciler');
        $this->db->where('kitaplar_id', $bilgiler['kitap_id'])->set('okunma', "1")->update('kitaplar');
        $ogrenciid = $bilgiler['ogrenci_id'];
        $this->db->query("UPDATE guven SET aldigi_kitaps = aldigi_kitaps+1 WHERE ogrenci_id = $ogrenciid");
        $this->db->trans_complete();
        if($this->db->trans_status() == TRUE){
            return "1";
        }else{
            return "0";
        }
    }

    public function kitapteslim($bilgiler)
    {
        $dizi = array(
            "teslim_edildimi" => 0,
            "ogrenci_id" => $bilgiler['ogrenci'],
            "kitap_id" => $bilgiler['kitap']
        );
        $this->db->trans_start();
        $this->db->where($dizi)->update("kutuphane", $bilgiler['kutuphane']);
        $this->db->where("ogrenciler_id", $bilgiler['ogrenci'])->set("kitap_varmi", "0")->update("ogrenciler");
        $this->db->where("kitaplar_id", $bilgiler['kitap'])->set("okunma", "0")->update("kitaplar");
        $gkontrol = $this->db->where(array(
            "ogrenci_id" => $bilgiler['ogrenci'],
            "kitap_id" => $bilgiler['kitap']
        ))->order_by('teslim_tarihi DESC')->limit(1)->select("teslim_tarihi,tahmini_teslim_tarihi")->get("kutuphane");
        $guven = $gkontrol->row_array();
        $ttt = explode(" ", $guven['tahmini_teslim_tarihi']);
        $tt = $guven['teslim_tarihi'];
        $ogrenci = $bilgiler['ogrenci'];
        if ($ttt[0] < $tt) {
            $this->db->query("UPDATE guven SET gec_teslims = gec_teslims+1 WHERE ogrenci_id = $ogrenci");
        } else {
            $this->db->query("UPDATE guven SET zamaninda_teslims = zamaninda_teslims+1 WHERE ogrenci_id = $ogrenci");
        }
        $this->db->trans_complete();
        if($this->db->trans_status() == TRUE){
            return "1";
        }else{
            return "0";
        }
    }

    public function kitaplar()
    {
        $kitaplar = $this->db->join("turler", "kitaplar.kitap_tur = turler.tur_id", "inner")->where('okunma', 0)->or_where('okunma', NULL)->get("kitaplar");
        $data = $kitaplar->result_array();
        return $data;
    }
}