<?php

/**
 * Created by PhpStorm.
 * User: Batuhan
 * Date: 31.03.2017
 * Time: 21:23
 */
class Idare_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    ## Anasayfa ##
    public function tos()
    {
        $tos = $this->db->query("SELECT COUNT(*) FROM ogrenciler");
        if ($this->db->affected_rows() >= 1) {
            return $tos->row_array();
        } else {
            return array(
                "COUNT(*)" => "0"
            );
        }
    }

    public function tks()
    {
        $tks = $this->db->query("SELECT COUNT(*) FROM kitaplar");
        if ($this->db->affected_rows() >= 1) {
            return $tks->row_array();
        } else {
            return array(
                "COUNT(*)" => "0"
            );
        }
    }

    public function oks()
    {
        $oks = $this->db->query("SELECT COUNT(*) FROM kitaplar WHERE okunma = 1");
        if ($this->db->affected_rows() >= 1) {
            return $oks->row_array();
        } else {
            return array(
                "COUNT(*)" => "0"
            );
        }
    }

    public function gks()
    {
        $gks = $this->db->query("SELECT COUNT(*) FROM kutuphane WHERE DATEDIFF(NOW(),tahmini_teslim_tarihi) >= 1 and teslim_edildimi = 0");
        if ($this->db->affected_rows() >= 1) {
            return $gks->row_array();
        } else {
            return array(
                "COUNT(*)" => "0"
            );
        }
    }

    ## Giriş ##
    public function giris($bilgi)
    {
        $giris = $this->db->where($bilgi)->get("idare");
        if ($this->db->affected_rows() >= 1) {
            $data['islem'] = 1;
            $data['sonuc'] = $giris->row_array();
        } else {
            $data['islem'] = 0;
        }
        return $data;
    }

    ## Öğrenci İşlemleri ##
    public function ogrenciler()
    {
        $ogrenciler = $this->db->join("guven", "ogrenciler.ogrenciler_id = guven.ogrenci_id", "inner")->get("ogrenciler");
        $data = $ogrenciler->result_array();
        return $data;
    }

    public function osil($id)
    {
        $id = $this->security->xss_clean($id);
        $dizi = array(
            "ogrenci_id" => $id,
            "teslim_edildimi" => 0
        );
        $kontrol = $this->db->where($dizi)->get("kutuphane");
        if ($this->db->affected_rows() >= 1) {
            return "2";
        } else {
            $this->db->delete("ogrenciler", array('ogrenciler_id' => $id));
            if ($this->db->affected_rows() >= 1) {
                return "1";
            } else {
                return "0";
            }
        }
    }

    public function obak($id)
    {
        $id = $this->security->xss_clean($id);
        $obak = $this->db->where("ogrenciler.ogrenciler_id", $id)->join("guven", "ogrenciler.ogrenciler_id = guven.ogrenci_id", "inner")->get("ogrenciler");
        if ($this->db->affected_rows() >= 1) {
            $data['data'] = $obak->row_array();
            $data['islem'] = "1";
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function obul($id)
    {
        $id = $this->security->xss_clean($id);
        $obul = $this->db->where("ogrenciler_id", $id)->get("ogrenciler");
        if ($this->db->affected_rows() >= 1) {
            $data['data'] = $obul->row_array();
            $data['islem'] = "1";
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function oduz($bilgi)
    {
        $this->db->where("ogrenciler_id", $bilgi['id'])->update("ogrenciler", $bilgi['data']);
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }

    public function oekle($bilgi)
    {
        $kontrol = $this->db->where("no", $bilgi['no'])->or_where("okod", $bilgi['okod'])->get("ogrenciler");
        if ($this->db->affected_rows() >= 1) {
            return "2";
        } else {
            $this->db->trans_start();
            $this->db->insert("ogrenciler", $bilgi);
            $id = $this->db->insert_id();
            $this->db->set("ogrenci_id", $id)->insert("guven");
            $this->db->trans_complete();
            if($this->db->trans_status() == TRUE){
                return "1";
            }else{
                return "0";
            }
        }
    }

    ## Kutuphane İşlemleri ##
    public function kao()
    {
        $kao = $this->db->join("ogrenciler", "kutuphane.ogrenci_id = ogrenciler.ogrenciler_id", "inner")->join("kitaplar", "kutuphane.kitap_id = kitaplar.kitaplar_id")->get("kutuphane");
        $data = $kao->result_array();
        return $data;
    }

    public function ggk()
    {
        $where = array(
            "DATEDIFF(NOW(),kutuphane.tahmini_teslim_tarihi) >=" => 1,
            "teslim_edildimi" => 0
        );
        $kutuphane = $this->db->join("ogrenciler", "kutuphane.ogrenci_id = ogrenciler.ogrenciler_id", "inner")->join("kitaplar", "kutuphane.kitap_id = kitaplar.kitaplar_id")->where($where)->get("kutuphane");
        $data = $kutuphane->result_array();
        return $data;
    }

    ## Tür İşlemleri ##
    public function turler($id = null)
    {
        if (!empty($id) and is_numeric($id)) {
            $id = $this->security->xss_clean($id);
            $turler = $this->db->where("tur_id", $id)->get("turler");
            if ($this->db->affected_rows() >= 1) {
                $data = $turler->row_array();
                $data['kod'] = 1;
            } else {
                $data['kod'] = 0;
            }
        } else {
            $turler = $this->db->get("turler");
            $data = $turler->result_array();
        }

        return $data;
    }

    public function tsil($id)
    {
        $id = $this->security->xss_clean($id);
        $this->db->delete("turler", array('tur_id' => $id));
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }

    public function tduz($bilgi)
    {
        $this->db->where("tur_id", $bilgi['id'])->update("turler", $bilgi['data']);
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }

    public function tekle($bilgi)
    {
        $kontrol = $this->db->where("tur_adi", $bilgi['tur_adi'])->get("turler");
        if ($this->db->affected_rows() >= 1) {
            return "2";
        } else {
            $this->db->insert("turler", $bilgi);
            if ($this->db->affected_rows() >= 1) {
                return "1";
            } else {
                return "0";
            }
        }
    }

    ## Kitap İşlemleri ##
    public function ksil($id)
    {
        $id = $this->security->xss_clean($id);
        $dizi = array(
            "kitaplar_id" => $id,
            'okunma' => 0
        );
        $kontrol = $this->db->where($dizi)->get("kitaplar");
        if ($this->db->affected_rows() >= 1) {
            $data = $kontrol->row_array();
            $tur_id = $data['kitap_tur'];
            $this->db->trans_start();
            $this->db->delete("kitaplar", array('kitaplar_id' => $id));
            $this->db->query("UPDATE turler SET kitaps = (kitaps - 1) WHERE tur_id = $tur_id");
            $this->db->trans_complete();
            if($this->db->trans_status() == TRUE){
                return "1";
            }else{
                return "0";
            }
        } else {
            return "2";
        }
    }

    public function skbul($id)
    {
        $id = $this->security->xss_clean($id);
        $kbul = $this->db->where("ogrenci_id", $id)->join("kitaplar", "kutuphane.kitap_id = kitaplar.kitaplar_id")->order_by("id DESC")->limit(1)->get("kutuphane");
        if ($this->db->affected_rows() >= 1) {
            $data['islem'] = "1";
            $data['data'] = $kbul->row_array();
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function kbul($id)
    {
        $id = $this->security->xss_clean($id);
        $kbul = $this->db->where("kitaplar_id", $id)->get("kitaplar");
        if ($this->db->affected_rows() >= 1) {
            $data = $kbul->row_array();
            $data['islem'] = "1";
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function kitaplar()
    {
        $kitaplar = $this->db->join("turler", "kitaplar.kitap_tur = turler.tur_id", "inner")->get("kitaplar");
        $data = $kitaplar->result_array();
        return $data;
    }

    public function kekle($bilgi)
    {
        $this->db->trans_start();
        $this->db->insert("kitaplar", $bilgi);
        $tur_id = $this->security->xss_clean($bilgi['kitap_tur']);
        $this->db->query("UPDATE turler SET kitaps = (kitaps + 1) WHERE tur_id = $tur_id");
        $this->db->trans_complete();
        if($this->db->trans_status() == TRUE){
            return "1";
        }else{
            return "0";
        }
    }

    public function kduz($bilgi)
    {
        $this->db->where("kitaplar_id", $bilgi['id'])->update("kitaplar", $bilgi['data']);
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }

    ## İdare İşlemleri ##
    public function iekle($bilgi){
        $this->db->insert("idare", $bilgi);
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }

    public function iduz($bilgi){
        $this->db->where("idare_id", $bilgi['id'])->update("idare", $bilgi['data']);
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }

    public function ibul($id){
        $id = $this->security->xss_clean($id);
        $ibul = $this->db->where("idare_id", $id)->get("idare");
        if ($this->db->affected_rows() >= 1) {
            $data = $ibul->row_array();
            $data['islem'] = "1";
        } else {
            $data['islem'] = "0";
        }
        return $data;
    }

    public function ihesaplar(){
        $idare = $this->db->get("idare");
        $data = $idare->result_array();
        return $data;
    }

    public function isil($id){
        $id = $this->security->xss_clean($id);
        $this->db->delete("idare", array('idare_id' => $id));
        if ($this->db->affected_rows() >= 1) {
            return "1";
        } else {
            return "0";
        }
    }
}