<div class="card-content black-text">
    <span class="card-title">Kitap Teslim Et</span>
    <div class="row">
        <form class="col s12" action="" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <input id="okod" readonly name="okod" placeholder="" type="text" class="validate invalid" required autofocus>
                    <label for="okod" data-error="Lütfen kartınızı okutun.">Öğrenci Kart Kod</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="" readonly id="isbn" name="isbn" type="number" class="validate invalid" required>
                    <label for="isbn" data-error="Lütfen kitabın barkodunu okutun.">Kitap Barkod</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Teslim Et</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>