<div class="card-content black-text">
    <span class="card-title">Kitaplar</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">#</th>
                <th>Kitap Adı</th>
                <th data-breakpoints="xs">Kitap Yazarı</th>
                <th data-breakpoints="xs">Kitap Türü</th>
                <th data-sortable="false">Kitap Konusu</th>
                <th data-breakpoints="xs" data-type="number">Tasnif No</th>
                <th data-breakpoints="xs" data-sortable="false" data-filterable="false">Sıra No</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($kitaplar as $row):
                ?>
                <tr>
                    <td><?= $row['kitaplar_id']; ?></td>
                    <td><?= $row['kitap_adi']; ?></td>
                    <td><?= $row['kitap_yazar']; ?></td>
                    <td><?= $row['tur_adi']; ?></td>
                    <td><?= $row['kitap_konu']; ?></td>
                    <td><?= $row['tasnif']; ?></td>
                    <td><?= $row['sira']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>