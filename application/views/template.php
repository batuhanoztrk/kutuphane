<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta charset="utf-8">
    <title><?= get_instance()->config->config['title']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Batuhan Öztürk">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/footable.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/materialize.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/style.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="<?= base_url(); ?>theme/img/favicon.ico">
</head>

<body>
<nav>
    <div class="nav-wrapper">
        <a href="<?= base_url(); ?>" class="brand-logo baslik"><?= get_instance()->config->config['title']; ?></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li class="activeLinkControla"><a href="<?= base_url(); ?>">Kitap Al</a></li>
            <li class="activeLinkControl"><a href="<?= base_url(); ?>kteslim">Kitap Teslim Et</a></li>
            <li class="activeLinkControl"><a href="<?= base_url(); ?>kitaplar">Kitaplar</a></li>
            <li class="activeLinkControl"><a href="<?= base_url(); ?>enler">Enler</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li class="activeLinkControla"><a href="<?= base_url(); ?>">Kitap Al</a></li>
            <li class="activeLinkControl"><a href="<?= base_url(); ?>kteslim">Kitap Teslim Et</a></li>
            <li class="activeLinkControl"><a href="<?= base_url(); ?>kitaplar">Kitaplar</a></li>
            <li class="activeLinkControl"><a href="<?= base_url(); ?>enler">Enler</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card grey lighten-4">
                <?php
                echo $icerik;
                $buyil = date("Y");
                if ($buyil == '2017') {
                    $tarih = "2017";
                } else {
                    $tarih = "2017–$buyil";
                }
                ?>
            </div>
        </div>
    </div>
</div>

<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © <?= $tarih; ?> Tüm Hakları Saklıdır.
        </div>
    </div>
</footer>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="<?= base_url(); ?>theme/js/jquery-3.2.1.min.js"></script>
<script src="<?= base_url(); ?>theme/js/materialize.min.js"></script>
<script src="<?= base_url(); ?>theme/js/jquery.scannerdetection.min.js"></script>
<script src="<?= base_url(); ?>theme/js/footable.min.js"></script>
<script src="<?= base_url(); ?>theme/js/main.min.js"></script>
</body>

</html>

