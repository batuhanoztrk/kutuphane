<div class="card-content black-text">
    <span class="card-title">Toplu Tür Ekle</span>
    <div class="row">
        <form id="topluEkle" class="col s12" method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn">
                        <span>Dosya Seç</span>
                        <input id="excel" accept=".xlsx,.xls,.ods" name="excel" type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input placeholder="Türlerin Olduğu Dosya (.xls|.xlsx|.ods)" class="file-path validate"
                               type="text" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="sutun" name="sutun" placeholder="A" type="text" class="validate" required>
                    <label for="sutun" data-error="Lütfen sütunu girin.">Türlerin Olduğu Sütun</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Ekle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>