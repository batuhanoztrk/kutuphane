<div class="card-content black-text">
    <span class="card-title">İdare Hesapları</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">#</th>
                <th>Kullanıcı Adı</th>
                <th>E-Posta</th>
                <th data-type="html" data-filterable="false" data-sortable="false">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($hesaplar as $row):
                ?>
                <tr>
                    <td><?= $row['idare_id']; ?></td>
                    <td><?= $row['kadi']; ?></td>
                    <td><?= $row['eposta']; ?></td>
                    <td>
                        <a class="btn btn-a green darken-4" title="Düzenle"
                           href="<?= base_url(); ?>idare/iduz/<?= $row['idare_id']; ?>">
                            <i class="material-icons">edit</i>
                        </a>
                        <a class="btn btn-a red darken-4" title="Sil" onclick="return confirm('İdare hesabı silinecek')"
                           href="<?= base_url(); ?>idare/isil/<?= $row['idare_id']; ?>">
                            <i class="material-icons">clear</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>