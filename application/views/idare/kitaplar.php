<div class="card-content black-text">
    <span class="card-title">Kitaplar</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">#</th>
                <th>Kitap Adı</th>
                <th data-breakpoints="xs">Kitap Yazarı</th>
                <th data-breakpoints="xs">Kitap Türü</th>
                <th data-sortable="false">Kitap Konusu</th>
                <th data-type="number" data-breakpoints="xs" data-sortable="false">ISBN</th>
                <th data-breakpoints="xs" data-type="number">Tasnif No</th>
                <th data-breakpoints="xs" data-sortable="false" data-filterable="false">Sıra No</th>
                <th data-filterable="false" data-breakpoints="xs">Okunma Sayısı</th>
                <th data-filterable="false" data-sortable="false">Okuyan Var mı?</th>
                <th data-type="html" data-filterable="false" data-sortable="false">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($kitaplar as $row):
                ?>
                <tr>
                    <td><?= $row['kitaplar_id']; ?></td>
                    <td><?= $row['kitap_adi']; ?></td>
                    <td><?= $row['kitap_yazar']; ?></td>
                    <td><?= $row['tur_adi']; ?></td>
                    <td><?= $row['kitap_konu']; ?></td>
                    <td><?= $row['isbn']; ?></td>
                    <td><?= $row['tasnif']; ?></td>
                    <td><?= $row['sira']; ?></td>
                    <td><?= okunma_sayisi($row['kitaplar_id']); ?></td>
                    <td>
                        <?php
                        if ($row['okunma'] == 0) {
                            echo 'Yok';
                        } else {
                            echo 'Var';
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-a green darken-4" title="Düzenle"
                           href="<?= base_url(); ?>idare/kduz/<?= $row['kitaplar_id']; ?>">
                            <i class="material-icons">edit</i>
                        </a>
                        <a class="btn btn-a red darken-4" title="Sil" onclick="return confirm('Kitap silinecek')"
                           href="<?= base_url(); ?>idare/ksil/<?= $row['kitaplar_id']; ?>">
                            <i class="material-icons">clear</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>