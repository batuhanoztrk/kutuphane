<div class="card-content black-text">
    <span class="card-title">Toplu Kitap Ekle</span>
    <div class="row">
        <form id="topluEkle" class="col s12" method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <div class="file-field input-field">
                    <div class="btn">
                        <span>Dosya Seç</span>
                        <input id="excel" accept=".xlsx,.xls,.ods" name="excel" type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input placeholder="Yeni Eklenecek Kitapların Olduğu Dosya (.xls|.xlsx|.ods)"
                               class="file-path validate" type="text" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="sutun" name="sutun" placeholder="A" type="text" class="validate" required>
                    <label for="sutun" data-error="Lütfen sütunu girin.">Kitap Adlarının Olduğu Sütun</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="B" id="sutun2" name="sutun2" type="text" class="validate" required>
                    <label for="sutun2" data-error="Lütfen sütunu girin.">Kitap Yazarlarının Olduğu Sütun</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="C" id="sutun6" name="sutun6" type="text" class="validate">
                    <label for="sutun6" data-error="Lütfen sütunu girin.">Kitap Konularının Olduğu Sütun</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="D" id="sutun3" name="sutun3" type="text" class="validate" required>
                    <label for="sutun3" data-error="Lütfen sütunu girin.">Uluslararası Standart Kitap Numaralarının
                        (ISBN) Olduğu Sütun</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="E" id="sutun4" name="sutun4" type="text" class="validate" required>
                    <label for="sutun4" data-error="Lütfen sütunu girin.">Tasnif Numaralarının Olduğu Sütun</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="F" id="sutun5" name="sutun5" type="text" class="validate" required>
                    <label for="sutun5" data-error="Lütfen sütunu girin.">Sıra Numaralarının Olduğu Sütun</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <select id="tur" name="tur">
                        <?php
                        foreach ($turler as $tur):
                            ?>
                            <option value="<?= $tur['tur_id']; ?>"><?= $tur['tur_adi']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <label for="tur">Kitapların Ekleneceği Tür</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Ekle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>