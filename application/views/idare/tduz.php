<div class="card-content black-text">
    <span class="card-title">Tür Düzenle</span>
    <div class="row">
        <form class="col s12" method="post" action="">
            <div class="row">
                <div class="input-field col s12">
                    <input id="ad" value="<?= $tur['tur_adi']; ?>" name="ad" placeholder="" type="text" class="validate"
                           required autofocus>
                    <label for="ad" data-error="Lütfen tür adını girin.">Tür Adı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Düzenle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>