<div class="card-content black-text">
    <audio id="qrkod_wav" src="<?= base_url(); ?>/theme/qrkod.wav" type="audio/wav" preload="auto"></audio>
    <span class="card-title">Kitap Ekle</span>
    <div class="row">
        <form class="col s12" method="post" action="">
            <div class="row">
                <div class="input-field col s12">
                    <input id="ad" name="ad" placeholder="" value="<?= $kitap['kitap_adi']; ?>" type="text"
                           class="validate" required autofocus>
                    <label for="ad" data-error="Lütfen kitabın adını girin.">Kitap Adı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="" value="<?= $kitap['kitap_yazar']; ?>" id="yazar" name="yazar" type="text"
                           class="validate" required>
                    <label for="yazar" data-error="Lütfen kitabın yazarını girin.">Kitap Yazarı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea placeholder="Lütfen kitabın konusunu girin." id="konu" name="konu" class="materialize-textarea"><?= $kitap['kitap_konu']; ?></textarea>
                    <label for="konu">Kitap Konusu</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" value="<?= $kitap['isbn']; ?>" id="isbn" name="isbn" type="number"
                           class="validate valid" required>
                    <label for="isbn" data-error="Lütfen kitabın uluslararası standar kitap numarasını (ISBN) girin.">ISBN</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" value="<?= $kitap['tasnif']; ?>" id="tasnif" name="tasnif" type="number" class="validate" required>
                    <label for="tasnif" data-error="Lütfen kitabın tasnif numarasını girin.">Tasnif No</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" value="<?= $kitap['sira']; ?>" id="sira" name="sira" type="number" class="validate" required>
                    <label for="sira" data-error="Lütfen kitabın sıra numarasını girin.">Sıra No</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <select id="tur" name="tur">
                        <?php

                        foreach ($turler as $tur) {
                            if ($tur['tur_id'] == $kitap['kitap_tur']) {
                                echo '<option selected value=' . $tur['tur_id'] . '>' . $tur['tur_adi'] . '</option>';
                            } else {
                                echo '<option value=' . $tur['tur_id'] . '>' . $tur['tur_adi'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <label for="tur">Kitap Türü</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Düzenle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>