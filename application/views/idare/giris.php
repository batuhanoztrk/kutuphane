<div class="card-content black-text">
    <span class="card-title">Giriş Yap</span>
    <div class="row">
        <form class="col s12" method="post" action="">
            <div class="row">
                <div class="input-field col s12">
                    <input id="kadi" name="kadi" placeholder="" type="text" class="validate" required autofocus>
                    <label for="kadi" data-error="Lütfen kullanıcı adını girin.">Kullanıcı Adı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" id="sifre" name="sifre" type="password" class="validate" required>
                    <label for="no" data-error="Lütfen şifrenizi girin.">Şifre</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Giriş Yap</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>