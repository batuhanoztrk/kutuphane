<div class="card-content black-text">
    <span class="card-title">Kitap Ekle</span>
    <div class="row">
        <form class="col s12" method="post" action="">
            <div class="row">
                <div class="input-field col s12">
                    <input id="ad" name="ad" placeholder="" type="text" class="validate" required autofocus>
                    <label for="ad" data-error="Lütfen kitabın adını girin.">Kitap Adı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="" id="yazar" name="yazar" type="text" class="validate" required>
                    <label for="yazar" data-error="Lütfen kitabın yazarını girin.">Kitap Yazarı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea placeholder="Lütfen kitabın konusunu girin." id="konu" name="konu" class="materialize-textarea"></textarea>
                    <label for="konu">Kitap Konusu</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" id="isbn" name="isbn" type="number" class="validate invalid" required>
                    <label for="isbn" data-error="Lütfen kitabın uluslararası standar kitap numarasını (ISBN) girin.">ISBN</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" id="tasnif" name="tasnif" type="number" class="validate" required>
                    <label for="tasnif" data-error="Lütfen kitabın tasnif numarasını girin.">Tasnif No</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" id="sira" name="sira" type="number" class="validate" required>
                    <label for="sira" data-error="Lütfen kitabın sıra numarasını girin.">Sıra No</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <select id="tur" name="tur">
                        <?php
                        foreach ($turler as $tur):
                            ?>
                            <option value="<?= $tur['tur_id']; ?>"><?= $tur['tur_adi']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <label for="tur">Kitap Türü</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Ekle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>
