<div class="card-content black-text">
    <span class="card-title">Öğrenci Düzenle</span>
    <div class="row">
        <form class="col s12" method="post" action="">
            <div class="row">
                <div class="input-field col s12">
                    <input id="adsoyad" value="<?= $ogrenci['data']['ad_soyad']; ?>" name="adsoyad" placeholder=""
                           type="text" class="validate" required autofocus>
                    <label for="adsoyad" data-error="Lütfen öğrencinin adını soyadını girin.">Öğrenci Ad Soyad</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input min=1 placeholder="" id="no" name="no" value="<?= $ogrenci['data']['no']; ?>" type="number"
                           class="validate" required>
                    <label for="no" data-error="Lütfen öğrencinin okul numarasını girin.">Öğrenci Okul No</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="okod" value="<?= $ogrenci['data']['okod']; ?>" name="okod" placeholder="" type="text"
                           class="validate valid" required>
                    <label for="okod" data-error="Lütfen kartınızı okutun veya kart kodunu girin.">Öğrenci Kart
                        Kod</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Düzenle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>
