<div class="card-content black-text">
    <span class="card-title">Yönetim Paneli</span>
    <div class="row">
        <div class="col s12 m6">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s4">
                        <i class="fa fa-users fa-3x" style="color: blue;" aria-hidden="true"></i>
                    </div>
                    <div class="col s8">
              <span class="black-text">
                Toplam Öğrenci Sayısı
                  <br><b><?= $tos['COUNT(*)']; ?></b>
              </span>
                    </div>
                </div>
                <div class="card-action">
                    <a href="<?= base_url() . "idare/ogrenciler"; ?>" class="blue-text" style="">Öğrenciler</a>
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s4">
                        <i class="fa fa-book fa-3x" style="color: darkorange;" aria-hidden="true"></i>
                    </div>
                    <div class="col s8">
              <span class="black-text">
                Toplam Kitap Sayısı
                  <br><b><?= $tks['COUNT(*)']; ?></b>
              </span>
                    </div>
                </div>
                <div class="card-action">
                    <a href="<?= base_url() . "idare/kitaplar"; ?>" class="blue-text">Kitaplar</a>
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s4">
                        <i class="fa fa-check fa-3x" style="color:green;" aria-hidden="true"></i>
                    </div>
                    <div class="col s8">
              <span class="black-text">
                Öğrencide Olan Kitap Sayısı
                  <br><b><?= $oks['COUNT(*)']; ?></b>
              </span>
                    </div>
                </div>
                <div class="card-action">
                    <a href="<?= base_url() . "idare/kao"; ?>" class="blue-text">Kitap Alan Öğrenciler</a>
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s4">
                        <i class="fa fa-times fa-3x" style="color: red;" aria-hidden="true"></i>
                    </div>
                    <div class="col s8">
              <span class="black-text">
                Getirilmeyen Kitap Sayısı
                  <br><b><?= $gks['COUNT(*)']; ?></b>
              </span>
                    </div>
                </div>
                <div class="card-action">
                    <a href="<?= base_url() . "idare/ggk"; ?>" class="blue-text">Geri Getirilmeyen Kitaplar</a>
                </div>
            </div>
        </div>
        <div class="col s12 m12">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s4">
                        <i class="fa fa-file-excel-o fa-3x" style="color:#1f7246;" aria-hidden="true"></i>
                    </div>
                    <div class="col s8">
              <span class="black-text">
                Yıl Sonu Raporu
              </span>
                    </div>
                </div>
                <div class="card-action">
                    <select id="yilsonu">
                    <option value="" disabled selected>Yıl Seçiniz</option>
                    <?php 
                    $buyil = date("Y");
                        for($i = ($buyil-5); $i <= $buyil; $i++){
                            echo "<option value='$i'>$i</option>";
                        }
                    ?>
                    </select>
                    <a href="" baseurl ="<?=base_url();?>" id="yilsonua" class="blue-text not-active">Yıl Sonu Raporu Oluştur</a>
                </div>
            </div>
        </div>
        <table class="striped">
            <thead>
            <tr>
                <th>En Çok Kitap Okuyan Öğrenci</th>
                <th>En Az Kitap Okuyan Öğrenci</th>
                <th>Hiç Kitap Okumayan Öğrenci</th>
                <th>En Çok Okunan Kitap</th>
            </tr>
            </thead>
            <tbody>
            <td><?php
                $url = base_url() . "idare/obak/";
                if (isset($eco['ogrenciler_id'])) {
                    echo '<a href="' . $url . $eco['ogrenciler_id'] . '">' . $eco['ad_soyad'] . ' <small style="color:grey">(' . $eco['aldigi_kitaps'] . ' Kitap okudu)</small></a>';
                } else {
                    echo "Kitap okuyan öğrenci yok";
                }
                ?></td>
            <td><?php
                if (isset($eao['ogrenciler_id'])) {
                    echo '<a href="' . $url . $eao['ogrenciler_id'] . '">' . $eao['ad_soyad'] . ' <small style="color:grey">(' . $eao['aldigi_kitaps'] . ' Kitap okudu)</small></a>';
                } else {
                    echo "Ortalamanın altında kitap okuyan öğrenci yok";
                }
                ?></td>
            <td><?php
                if (isset($ho['ogrenciler_id'])) {
                    echo '<a href="' . $url . $ho['ogrenciler_id'] . '">' . $ho['ad_soyad'] . '</a>';
                } else {
                    echo "Kitap okumayan öğrenci yok";
                }
                ?></td>
            <td><?php
                if (isset($eck['kitap_adi'])) {
                    echo $eck['kitap_adi'];
                } else {
                    echo "Daha önce hiç kitap okunmamış";
                }
                ?></td>
            </tbody>
        </table>    
    </div>
</div>