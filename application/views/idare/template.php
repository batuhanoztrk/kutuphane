<?php
$giris_url = $_SERVER['REQUEST_URI'];
$giris_parc = explode('/', $giris_url);
$giris_s = count($giris_parc) - 1;
if (!isset($_SESSION['kullanici']) and $giris_parc[$giris_s] != "giris") {
    redirect(base_url() . "idare/giris", "location");
}
?>
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta charset="utf-8">
    <title><?= get_instance()->config->config['title']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Batuhan Öztürk">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/footable.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/materialize.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>theme/css/style.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="<?= base_url(); ?>theme/img/favicon.ico">
</head>

<body>
<ul id="dropdown2" class="dropdown-content">
    <li><a href="<?= base_url() . "idare"; ?>">Anasayfa</a></li>
    <li class="divider"></li>
    <li><a href="<?= base_url(); ?>" target="_blank">Siteyi Görüntüle</a></li>
</ul>
<?php if(isset($_SESSION['kullanici'])):  ?>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="<?= base_url() . "idare/ogrenciler"; ?>">Öğrenciler</a></li>
    <li><a href="<?= base_url() . "idare/oekle"; ?>">Tek Öğrenci Ekle</a></li>
    <li><a href="<?= base_url() . "idare/toekle"; ?>">Toplu Öğrenci Ekle</a></li>
</ul>
<ul id="dropdown3" class="dropdown-content">
    <li><a href="<?= base_url() . "idare/turler"; ?>">Türler</a></li>
    <li><a href="<?= base_url() . "idare/tekle"; ?>">Tek Tür Ekle</a></li>
    <li><a href="<?= base_url() . "idare/ttekle"; ?>">Toplu Tür Ekle</a></li>
    <li class="divider"></li>
    <li><a href="<?= base_url() . "idare/kitaplar"; ?>">Kitaplar</a></li>
    <li><a href="<?= base_url() . "idare/kekle"; ?>">Tek Kitap Ekle</a></li>
    <li><a href="<?= base_url() . "idare/tkekle"; ?>">Toplu Kitap Ekle</a></li>
    <li class="divider"></li>
    <li><a href="<?= base_url() . "idare/kao"; ?>">Kitap Alan Öğrenciler</a></li>
    <li><a href="<?= base_url() . "idare/ggk"; ?>">Geri Getirilmeyen Kitaplar</a></li>
    <li class="divider"></li>
    <li><a href="<?= base_url() . "idare/dewey"; ?>">DEWEY Onlu Sınıflama Sistemi</a></li>
</ul>
<ul id="dropdown4" class="dropdown-content">
    <li><a href="<?= base_url() . "idare/ihesaplari"; ?>">Hesaplar</a></li>
    <li><a href="<?= base_url() . "idare/iekle"; ?>">Hesap Ekle</a></li>
    <li class="divider"></li>
    <li><a href="<?= base_url() . "idare/cikis"; ?>">Çıkış Yap</a></li>
</ul>
<?php endif; ?>
<nav>
    <div class="nav-wrapper">
        <a href="<?= base_url()?>idare"
           class="brand-logo baslik"><?= get_instance()->config->config['title']; ?></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Yönetim Paneli<i
                        class="material-icons right">arrow_drop_down</i></a></li>
            <?php
                if(isset($_SESSION['kullanici'])):
            ?>
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Öğrenci İşlemleri<i
                        class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdown3">Kütüphane İşlemleri<i
                        class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-button" href="#!" data-activates="dropdown4">Hesap İşlemleri<i
                            class="material-icons right">arrow_drop_down</i></a></li>
            <?php endif; ?>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li>
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header">Yönetim Paneli<i
                                class="material-icons right">arrow_drop_down</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="<?= base_url() . "idare"; ?>">Anasayfa</a></li>
                                <li class="divider"></li>
                                <li><a href="<?= base_url(); ?>" target="_blank">Siteyi Görüntüle</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <?php if(isset($_SESSION['kullanici'])): ?>
            <li>
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header">Öğrenci İşlemleri<i
                                class="material-icons right">arrow_drop_down</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="<?= base_url() . "idare/ogrenciler"; ?>">Öğrenciler</a></li>
                                <li><a href="<?= base_url() . "idare/oekle"; ?>">Tek Öğrenci Ekle</a></li>
                                <li><a href="<?= base_url() . "idare/toekle"; ?>">Toplu Öğrenci Ekle</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="collapsible collapsible-accordion">
                    <li>
                        <a class="collapsible-header">Kütüphane İşlemleri<i
                                class="material-icons right">arrow_drop_down</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="<?= base_url() . "idare/turler"; ?>">Türler</a></li>
                                <li><a href="<?= base_url() . "idare/tekle"; ?>">Tek Tür Ekle</a></li>
                                <li><a href="<?= base_url() . "idare/ttekle"; ?>">Toplu Tür Ekle</a></li>
                                <li class="divider"></li>
                                <li><a href="<?= base_url() . "idare/kitaplar"; ?>">Kitaplar</a></li>
                                <li><a href="<?= base_url() . "idare/kekle"; ?>">Tek Kitap Ekle</a></li>
                                <li><a href="<?= base_url() . "idare/tkekle"; ?>">Toplu Kitap Ekle</a></li>
                                <li class="divider"></li>
                                <li><a href="<?= base_url() . "idare/kao"; ?>">Kitap Alan Öğrenciler</a></li>
                                <li><a href="<?= base_url() . "idare/ggk"; ?>">Geri Getirilmeyen Kitaplar</a></li>
                                <li class="divider"></li>
                                <li><a href="<?= base_url() . "idare/dewey"; ?>">DEWEY Onlu Sınıflama Sistemi</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </li>
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li>
                            <a class="collapsible-header">Hesap İşlemleri<i
                                        class="material-icons right">arrow_drop_down</i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="<?= base_url() . "idare/ihesaplari"; ?>">Hesaplar</a></li>
                                    <li><a href="<?= base_url() . "idare/iekle"; ?>">Hesap Ekle</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?= base_url() . "idare/cikis"; ?>">Çıkış Yap</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card grey lighten-4">
                <?php
                echo $icerik;
                $buyil = date("Y");
                if ($buyil == '2017') {
                    $tarih = "2017";
                } else {
                    $tarih = "2017–$buyil";
                }
                ?>
            </div>
        </div>
    </div>
</div>

<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © <?= $tarih; ?> Tüm Hakları Saklıdır.
        </div>
    </div>
</footer>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="<?= base_url(); ?>theme/js/jquery-3.2.1.min.js"></script>
<script src="<?= base_url(); ?>theme/js/materialize.min.js"></script>
<script src="<?= base_url(); ?>theme/js/jquery.scannerdetection.min.js"></script>
<script src="<?= base_url(); ?>theme/js/footable.min.js"></script>
<script src="<?= base_url(); ?>theme/js/main.min.js"></script>
</body>

</html>