<div class="card-content black-text">
    <span class="card-title"><?= $ogrenci['ad_soyad']; ?> - <?= $ogrenci['no']; ?></span>
    <div class="row">
        <table class="table striped">
            <thead>
            <tr>
                <th data-breakpoints="xs">En Son Aldığı Kitap</th>
                <th>Toplam Aldığı Kitap Sayısı</th>
                <th>Zamanında Teslim Ettiği Kitap Sayısı</th>
                <th>Geç Teslim Ettiği Kitap Sayısı</th>
                <th data-breakpoints="xs">Şuan Okuduğu Kitap Var mı?</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= $kitap['kitap_adi']; ?></td>
                <td><?= $ogrenci['aldigi_kitaps']; ?></td>
                <td><?= $ogrenci['zamaninda_teslims']; ?></td>
                <td><?= $ogrenci['gec_teslims']; ?></td>
                <td>
                    <?php
                    if ($ogrenci['kitap_varmi'] == 1) {
                        echo 'Var';
                    } else {
                        echo 'Yok';
                    }
                    ?>
                </td>

            </tr>
            </tbody>
        </table>
    </div>
</div>