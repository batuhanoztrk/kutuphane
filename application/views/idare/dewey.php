<div class="card-content black-text">
    <span class="card-title">DEWEY Onlu Sınıflama Sistemi</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-type="number">NUMARA</th>
                <th>TÜRKÇE</th>
                <th>ENGLISH</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($kutuphanecilik as $dewey):
                ?>
                <tr>
                    <td><?= $dewey['A']; ?></td>
                    <td><?= $dewey['B']; ?></td>
                    <td><?= $dewey['C']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>