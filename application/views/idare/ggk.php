<div class="card-content black-text">
    <span class="card-title">Geri Getirilmeyen Kitaplar</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">#</th>
                <th data-sortable="false">Ad Soyad</th>
                <th data-breakpoints="xs" data-type="number">Okul No</th>
                <th data-sortable="false">Kitap Adı</th>
                <th data-breakpoints="xs" data-type="date">Alım Tarihi</th>
                <th data-type="date">Tahmini Teslim Tarihi</th>
                <th data-type="html" data-filterable="false" data-sortable="false">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($kutuphane as $row):
                $alim_tarihi = explode(" ", $row['alim_tarihi']);
                $tahmini_teslim_tarihi = explode(" ", $row['tahmini_teslim_tarihi']);
                $teslim_tarihi = explode(" ", $row['teslim_tarihi']);
                ?>
                <tr>
                    <td><?= $row['id']; ?></td>
                    <td><?= $row['ad_soyad']; ?></td>
                    <td><?= $row['no']; ?></td>
                    <td><?= $row['kitap_adi']; ?></td>
                    <td><?= $alim_tarihi[0]; ?></td>
                    <td><?= $tahmini_teslim_tarihi[0]; ?></td>
                    <td>
                        <a class="btn btn-a blue darken-4" title="Ayrıntılı Bak"
                           href="<?= base_url(); ?>idare/obak/<?= $row['ogrenciler_id']; ?>">
                            <i class="material-icons">zoom_in</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>