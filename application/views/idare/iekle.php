<div class="card-content black-text">
    <span class="card-title">İdare Hesabı Ekle</span>
    <div class="row">
        <form class="col s12" method="post" action="">
            <div class="row">
                <div class="input-field col s12">
                    <input id="kadi" name="kadi" placeholder="" type="text" class="validate" required autofocus>
                    <label for="kadi" data-error="Lütfen kitabın adını girin.">Kullanıcı Adı</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="" id="eposta" name="eposta" type="email" class="validate" required>
                    <label for="eposta" data-error="Lütfen kitabın yazarını girin.">E-Posta</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="" id="sifre" name="sifre" type="password" class="validate" required>
                    <label for="sifre">Şifre</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn">Ekle</button>
                </div>
            </div>
        </form>
        <div class='col s12'>
            <?= validation_errors(); ?>
        </div>
    </div>
</div>
