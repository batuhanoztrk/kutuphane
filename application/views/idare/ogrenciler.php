<div class="card-content black-text">
    <span class="card-title">Öğrenciler</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">#</th>
                <th data-sortable="false">Ad Soyad</th>
                <th data-type="number">Okul No</th>
                <th data-sortable="false" data-breakpoints="xs">Kart Kodu</th>
                <th data-breakpoints="xs" data-sortable="false" data-filterable="false">Okuduğu Kitap Var mı?</th>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">Okuduğu Kitap Sayısı</th>
                <th data-type="html" data-filterable="false" data-sortable="false">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($ogrenciler as $ogrenci):
                ?>
                <tr>
                    <td><?= $ogrenci['ogrenciler_id']; ?></td>
                    <td><?= $ogrenci['ad_soyad']; ?></td>
                    <td><?= $ogrenci['no']; ?></td>
                    <td><?= $ogrenci['okod']; ?></td>
                    <td>
                        <?php
                        if ($ogrenci['kitap_varmi'] == 0) {
                            echo 'Yok';
                        } else {
                            echo 'Var';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $ogrenci['aldigi_kitaps'];
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-a blue darken-4" title="Ayrıntılı Bak"
                           href="<?= base_url(); ?>idare/obak/<?= $ogrenci['ogrenciler_id']; ?>">
                            <i class="material-icons">zoom_in</i>
                        </a>
                        <a class="btn btn-a green darken-4" title="Düzenle"
                           href="<?= base_url(); ?>idare/oduz/<?= $ogrenci['ogrenciler_id']; ?>">
                            <i class="material-icons">edit</i>
                        </a>
                        <a class="btn btn-a red darken-4" title="Sil" onclick="return confirm('Öğrenci silinecek')"
                           href="<?= base_url(); ?>idare/osil/<?= $ogrenci['ogrenciler_id']; ?>">
                            <i class="material-icons">clear</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>