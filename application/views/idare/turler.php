<div class="card-content black-text">
    <span class="card-title">Türler</span>
    <div class="row">
        <table class="table striped" data-sorting="true" data-paging="true" data-filtering="true"
               data-filter-placeholder="Ara...">
            <thead>
            <tr>
                <th data-breakpoints="xs" data-type="number" data-filterable="false">#</th>
                <th>Tür</th>
                <th data-filterable="false" data-type="number">Türe Ait Kitap Sayısı</th>
                <th data-type="html" data-filterable="false" data-sortable="false">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($turler as $row):
                ?>
                <tr>
                    <td><?= $row['tur_id']; ?></td>
                    <td><?= $row['tur_adi']; ?></td>
                    <td><?= $row['kitaps']; ?></td>
                    <td>
                        <a class="btn btn-a green darken-4" title="Düzenle"
                           href="<?= base_url(); ?>idare/tduz/<?= $row['tur_id']; ?>">
                            <i class="material-icons">edit</i>
                        </a>
                        <a class="btn btn-a red darken-4" title="Sil"
                           onclick="return confirm('Tür silinecek ve o türe ait kitaplar sistemde gözükmeyecek.')"
                           href="<?= base_url(); ?>idare/tsil/<?= $row['tur_id']; ?>">
                            <i class="material-icons">clear</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>