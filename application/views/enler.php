<div class="card-content black-text">
    <span class="card-title">Enler</span>
    <div class="row">
        <table class="striped">
            <thead>
            <tr>
                <th>En Çok Kitap Okuyan Öğrenci</th>
                <th>En Az Kitap Okuyan Öğrenci</th>
                <th>Hiç Kitap Okumayan Öğrenci</th>
                <th>En Çok Okunan Kitap</th>
            </tr>
            </thead>
            <tbody>
            <td><?php
                $url = base_url() . "idare/obak/";
                if (isset($eco['ogrenciler_id'])) {
                    echo $eco['ad_soyad'] . ' <small style="color:grey">(' . $eco['aldigi_kitaps'] . ' Kitap okudu)</small>';
                } else {
                    echo "Kitap okuyan öğrenci yok";
                }
                ?></td>
            <td><?php
                if (isset($eao['ogrenciler_id'])) {
                    echo $eao['ad_soyad'] . ' <small style="color:grey">(' . $eao['aldigi_kitaps'] . ' Kitap okudu)</small>';
                } else {
                    echo "Ortalamanın altında kitap okuyan öğrenci yok";
                }
                ?></td>
            <td><?php
                if (isset($ho['ogrenciler_id'])) {
                    echo $ho['ad_soyad'];
                } else {
                    echo "Kitap okumayan öğrenci yok";
                }
                ?></td>
            <td><?php
                    if(isset($eck['kitap_adi'])){
                        echo $eck['kitap_adi'];
                    }else{
                        echo "Daha önce hiç kitap okunmamış";
                    }
                ?></td>
            </tbody>
        </table>
    </div>
</div>