<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bilgi!</title>
    <style type="text/css">

        ::selection {
            background-color: #E13300;
            color: white;
        }

        ::-moz-selection {
            background-color: #E13300;
            color: white;
        }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;
        }

        p {
            margin: 12px 15px 12px 15px;
            color: #ffffff;
            width: 50%;
            border: 1px solid #fff;
            border-radius: 4px;
            padding: 4px 10px;
        }

        .basari {
            background-color: forestgreen;
        }

        .hata {
            background-color: #c71c22;
        }

        .uyari {
            background-color: #dd5600;
        }

        a {
            float: right;
            text-decoration: none;
            color: dodgerblue;
            border: 1px solid #9e9e9e;
            padding: 5px 5px 5px 5px;
            margin-top: -0.38em;
            border-radius: 5px;
            display: none;
        }

        a:hover {
            background-color: dodgerblue;
            color: white;
        }
    </style>
</head>
<body>
<div id="container">
    <h1>Bilgi! <a id="geri" href="<?= $_SERVER['HTTP_REFERER']; ?>">Geri Dön</a></h1>
    <?php
    foreach ($hata as $isl) {
        foreach ($isl as $is) {
            @$i = explode("&&", $is);
            @$ii = explode("%%", $i[1]);
            if (isset($ii[1]) and !empty($ii[1])) {
                $iii = explode("<br>", $ii[1]);
                $uri = base_url() . "idare/" . $iii[0];
                if ($ii[0] == 1) {
                    echo "<p class='basari'>" . $i[0] . "</p>";
                } elseif ($ii[0] == 2) {
                    echo "<p class='hata'>" . $i[0] . "</p>";
                } elseif ($ii[0] == 3) {
                    echo "<p class='uyari'>" . $i[0] . "</p>";
                }
            } else {
                if ($i[1] == 1) {
                    echo "<p class='basari'>" . $i[0] . "</p>";
                } elseif ($i[1] == 2) {
                    echo "<p class='hata'>" . $i[0] . "</p>";
                } elseif ($i[1] == 3) {
                    echo "<p class='uyari'>" . $i[0] . "</p>";
                }
            }

        }
    }
    if (isset($_SERVER['HTTP_REFERER'])) {
        @$url = $_SERVER['HTTP_REFERER'];
    } else {
        $url = base_url() . "idare";
    }
    if (isset($uri) and !empty($uri)) {
        header("Refresh: 30; url={$uri}");
    } else {
        header("Refresh: 30; url={$url}");
    }

    ?>
</div>
<script>
    setTimeout(function () {
        alert("30 Saniye içinde yönlendirileceksiniz...");
    }, 500);
    var msaniye = 700;
    setTimeout(function () {
        var e = document.getElementById("geri");
        e.style.display = 'block';
    }, msaniye);
</script>
</body>
</html>