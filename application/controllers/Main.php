<?php

/**
 * Created by PhpStorm.
 * User: Batuhan
 * Date: 31.03.2017
 * Time: 15:21
 */
class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url", "form"));
        $this->load->library("form_validation");
        $this->form_validation->set_error_delimiters("<div class='card-panel hata'>
                <strong>Hata !</strong> ", "</div>");
        $this->load->database();
        $this->load->model("main_model");
    }

    public function index()
    {
        $this->form_validation->set_rules('ttt', 'Tahmini Teslim Tarihi', "required");
        $this->form_validation->set_rules('okod', 'Öğrenci Kart Kod', 'trim|required');
        $this->form_validation->set_rules('isbn', 'Kitap Barkod', 'trim|required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $main['icerik'] = $this->load->view("main", '', TRUE);
            $this->load->view("template", $main);
        } else {
            $at = date("Y-m-d");
            $at2 = date("Y-m-d", strtotime("+31 days"));
            @$ttt = $this->input->post('ttt');
            @$okod = $this->input->post('okod', TRUE);
            @$isbn = $this->input->post('isbn', TRUE);
            $ogrenci = $this->main_model->qrogrenci($okod);
            $kitap = $this->main_model->qrkitap($isbn);
            if ($ogrenci['islem'] == 1) {
                if ($kitap['islem'] == 1) {
                    if ($ttt < $at2 and $ttt > $at) {
                        $bilgiler = array(
                            'ogrenci_id' => $ogrenci['sonuc']['ogrenciler_id'],
                            'kitap_id' => $kitap['sonuc']['kitaplar_id'],
                            'alim_tarihi' => $at,
                            'tahmini_teslim_tarihi' => $ttt
                        );
                        $ekle = $this->main_model->kitapal($bilgiler);
                        if ($ekle == 1) {
                            show_error("Kitap başarıyla alındı.", "", "Başarılı!");
                        } else {
                            show_error("Bir hata oluştu. Lütfen tekrar deneyin.", "", "Hata!");
                        }
                    } else {
                        show_error("Geçerli tarih aralığı seçiniz.", "", "Hata!");
                    }
                } else {
                    show_error("Barkodu okutulan kitap sisteme kayıtlı değil veya başkası tarafından okunuyor.", '', "Hata!");
                }
            } elseif ($ogrenci['islem'] == 2) {
                show_error("Şuan okuduğunuz bir kitap var. Önce onu teslim edin.", "", "Hata!");
            } else {
                show_error("Kartı okutulan öğrenci sisteme kayıtlı değil.", '', "Hata!");
            }
        }
    }

    public function kteslim()
    {
        $this->form_validation->set_rules('okod', 'Öğrenci Kart Kod', 'trim|required');
        $this->form_validation->set_rules('isbn', 'Kitap Barkod', 'trim|required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $main['icerik'] = $this->load->view("kteslim", '', TRUE);
            $this->load->view("template", $main);
        } else {
            @$okod = $this->input->post('okod', TRUE);
            @$isbn = $this->input->post('isbn', TRUE);
            $ogrenci = $this->main_model->qrogrenci($okod);
            $kitap = $this->main_model->kutuphanekitap($ogrenci['sonuc']['ogrenciler_id']);
            if ($ogrenci['islem'] == 2) {
                if ($kitap['islem'] == 2) {
                    if ($kitap['sonuc']['isbn'] == $isbn) {
                        $bt = date("Y-m-d");
                        $bilgiler['kutuphane'] = array(
                            "teslim_tarihi" => $bt,
                            "teslim_edildimi" => 1
                        );
                        $bilgiler['ogrenci'] = $ogrenci['sonuc']['ogrenciler_id'];
                        $bilgiler['kitap'] = $kitap['sonuc']['kitaplar_id'];
                        $ekle = $this->main_model->kitapteslim($bilgiler);
                        if ($ekle == 1) {
                            show_error("Kitap başarıyla teslim edildi.", "", "Başarılı!");
                        } else {
                            show_error("Bir hata oluştu. Lütfen tekrar deneyin.", "", "Hata!");
                        }
                    } else {
                        show_error("Barkodu okutulan kitap aldığınız kitap ile eşleşmiyor.", "", "Hata!");
                    }
                } elseif ($kitap['islem'] == 1) {
                    show_error("Bu kitabı şuan da okuyan yok.", "", "Hata!");
                } else {
                    show_error("Okutulan barkoda sahip kitap yok.", "", "Hata!");
                }
            } elseif ($ogrenci['islem'] == 1) {
                show_error("Şuan okuduğunuz kitap yok.", "", "Hata!");
            } else {
                show_error("Kartı okutulan öğrenci sisteme kayıtlı değil.", "", "Hata!");
            }
        }
    }

    public function enler()
    {
        $data = array(
            "eco" => $this->main_model->eco(),
            "eao" => $this->main_model->eao(),
            "ho" => $this->main_model->ho(),
            "eck" => $this->main_model->eck()
        );
        $enler['icerik'] = $this->load->view("enler", $data, TRUE);
        $this->load->view("template", $enler);
    }

    public function kitaplar()
    {
        $kitaplar['kitaplar'] = $this->main_model->kitaplar();
        $data['icerik'] = $this->load->view("kitaplar", $kitaplar, TRUE);
        $this->load->view("template", $data);
    }

}