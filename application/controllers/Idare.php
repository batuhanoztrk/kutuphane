<?php

/**
 * Created by PhpStorm.
 * User: Batuhan
 * Date: 31.03.2017
 * Time: 21:22
 */
class Idare extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array("url", "form", "kutuphane"));
        $this->load->library("form_validation");
        $this->form_validation->set_error_delimiters("<div class='card-panel hata'>
                <strong>Hata !</strong> ", "</div>");
       $this->load->database();
       $this->load->model("idare_model");
       $this->load->model("main_model");
        session_start();
    }

    ## Anasayfa ##
    public function index()
    {
        $data = array(
            "eco" => $this->main_model->eco(),
            "eao" => $this->main_model->eao(),
            "ho" => $this->main_model->ho(),
            "eck" => $this->main_model->eck(),
            "tos" => $this->idare_model->tos(),
            "tks" => $this->idare_model->tks(),
            "oks" => $this->idare_model->oks(),
            "gks" => $this->idare_model->gks()
        );
        $main['icerik'] = $this->load->view("idare/main", $data, TRUE);
        $this->load->view("idare/template", $main);
    }

    ## Giriş ve Çıkış ##
    public function giris()
    {
        if (isset($_SESSION['kullanici'])) {
            redirect(base_url() . "idare", "location");
        } else {
            $kural = 'trim|required';
            $this->form_validation->set_rules('kadi', 'Kullanıcı Adı', $kural);
            $this->form_validation->set_rules('sifre', 'Şifre', $kural);
            if ($this->form_validation->run() == FALSE) {
                $giris_yap['icerik'] = $this->load->view("idare/giris", '', TRUE);
                $this->load->view("idare/template", $giris_yap);
            } else {
                $kadi = $this->input->post("kadi", TRUE);
                $sifre = $this->input->post("sifre", TRUE);
                $sifre = md5(sha1($sifre));
                $bilgi = array(
                    "kadi" => $kadi,
                    "sifre" => $sifre
                );
                $giris = $this->idare_model->giris($bilgi);
                if ($giris['islem'] == 1) {
                    $_SESSION['kullanici'] = true;
                    $_SESSION['kadi'] = $giris['sonuc']['kadi'];
                    redirect(base_url() . "idare");
                } else {
                    show_error("Kullanıcı adı veya şifreniz hatalı. Lütfen tekrar deneyin!", "", "Hata!");
                }
            }
        }
    }

    public function cikis()
    {
        if (isset($_SESSION['kullanici'])) {
            session_destroy();
            redirect(base_url());
        } else {
            redirect(base_url() . "idare");
        }
    }

    ## Öğrenci İşlemleri ##
    public function oekle()
    {
        $kural = 'trim|required';
        $this->form_validation->set_rules('adsoyad', 'Öğrenci Ad Soyad', $kural);
        $this->form_validation->set_rules('no', 'Öğrenci Okul No', $kural . "|numeric");
        $this->form_validation->set_rules('okod', 'Öğrenci Kart Kod', "trim|required");
        if ($this->form_validation->run() == FALSE) {
            $oekle['icerik'] = $this->load->view("idare/oekle", '', TRUE);
            $this->load->view("idare/template", $oekle);
        } else {
            $adsoyad = $this->input->post("adsoyad", TRUE);
            $no = $this->input->post("no", TRUE);
            $okod = $this->input->post("okod", TRUE);
            $data = array(
                "ad_soyad" => $adsoyad,
                "no" => $no,
                "okod" => $okod
            );
            $ogrencie = $this->idare_model->oekle($data);
            if ($ogrencie == 1) {
                show_error("Öğrenci başarılı bir şekilde eklendi.", "", "Başarılı!");
            } elseif ($ogrencie == 2) {
                show_error("Aynı okul numarasına veya kart koduna sahip başka bir öğrenci var.", "", "Hata!");
            } else {
                show_error("Bir hata oluştu ve öğrenci eklenemedi. Lütfen tekrar deneyin.", "", "Hata!");
            }
        }
    }

    public function toekle()
    {
        set_time_limit(0);
        klasor_temizle("ogrenciler");
        $kural = 'trim|required';
        $this->form_validation->set_rules('sutun', 'Öğrenci Ad Soyadlarının Olduğu Sütun', $kural);
        $this->form_validation->set_rules('sutun2', 'Öğrenci Okul Numaralarının Olduğu Sütun', $kural);
        $this->form_validation->set_rules('okod', 'Öğrenci Kart Kodlarının Olduğu Sütun', $kural);
        if ($this->form_validation->run() == FALSE) {
            $toekle['icerik'] = $this->load->view("idare/toekle", '', TRUE);
            $this->load->view("idare/template", $toekle);
        } else {
            $adsoyad = strtoupper($this->input->post("sutun", TRUE));
            $no = strtoupper($this->input->post("sutun2", TRUE));
            $okod = strtoupper($this->input->post("okod", TRUE));
            $config['upload_path'] = 'ogrenciler/';
            $config['allowed_types'] = 'xls|xlsx|ods';
            $config['file_name'] = rand(0, 99999999);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('excel')) {
                show_error("Bir sorun oluştu ve dosya yüklenemedi.", "", "Hata!");
            } else {
                $this->load->library("phpexcel");
                $dosyadata = $this->upload->data();
                $dosya = "ogrenciler/" . $dosyadata['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($dosya);
                $excel_satirlar = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $excel_satirlar = array_map('array_filter', $excel_satirlar);
                $excel_satirlar = array_filter($excel_satirlar);
                foreach ($excel_satirlar as $excel_satir) {
                    $bilgiler = array(
                        "ad_soyad" => $excel_satir[$adsoyad],
                        "no" => $excel_satir[$no],
                        "okod" => $excel_satir[$okod]
                    );
                    $ekle = $this->idare_model->oekle($bilgiler);
                    if ($ekle == 1) {
                        $islem['hata'][] = array(
                            "sonuc" => "Öğrenci başarılı bir şekilde eklendi.&&1<br>"
                        );
                    } elseif ($ekle == 2) {
                        $islem['hata'][] = array(
                            "sonuc" => "<b>" . $excel_satir[$no] . "</b> okul numaralı öğrenci ile aynı okul numarasına veya kart koduna sahip başka bir öğrenci var.&&2<br>",
                        );
                    } else {
                        $islem['hata'][] = array(
                            "sonuc" => "<b>" . $excel_satir[$no] . " </b> okul numaralı öğrenci eklenirken bir hata oluştu ve öğrenci eklenemedi. Lütfen tekrar deneyin.&&2<br>"
                        );
                    }
                }
                $this->load->view("errors/html/error_toplu", $islem);
            }
        }
    }

    public function ogrenciler()
    {
        $data['ogrenciler'] = $this->idare_model->ogrenciler();
        $ogrenciler['icerik'] = $this->load->view("idare/ogrenciler", $data, TRUE);
        $this->load->view("idare/template", $ogrenciler);
    }

    public function osil($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/ogrenciler");
        } else {
            if (is_numeric($id)) {
                $sil = $this->idare_model->osil($id);
                if ($sil == 1) {
                    show_error("Öğrenci başarıyla silindi.", "", "Başarılı!");
                } elseif ($sil == 2) {
                    show_error("Öğrencinin aldığı kitabı teslim etmesi gerekiyor.", "", "Hata!");
                } else {
                    show_error("Bir hata oluştu ve öğrenci silinemedi. Lütfen tekrar deneyin.", "", "Hata!");
                }
            } else {
                $uri = "idare/ogrenciler";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    public function obak($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/ogrenciler");
        } else {
            if (is_numeric($id)) {
                $data = $this->idare_model->obak($id);
                if ($data['islem'] == 1) {
                    $kdata = $this->idare_model->skbul($id);
                    if ($kdata['islem'] == 1) {
                        $bilgi['kitap'] = $kdata['data'];
                    } else {
                        $bilgi['kitap']['kitap_adi'] = "Hiç kitap okumamış!";
                    }
                    $bilgi['ogrenci'] = $data['data'];
                    $obak['icerik'] = $this->load->view("idare/obak", $bilgi, TRUE);
                    $this->load->view("idare/template", $obak);
                } else {
                    $uri = "idare/ogrenciler";
                    show_error("Aranan öğrenci bulunamadı.&&$uri", "", "Hata!");
                }
            } else {
                $uri = "idare/ogrenciler";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    public function oduz($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/ogrenciler");
        } else {
            if (is_numeric($id)) {
                $kural = 'trim|required';
                $this->form_validation->set_rules('adsoyad', 'Öğrenci Ad Soyad', $kural);
                $this->form_validation->set_rules('no', 'Öğrenci Okul No', "trim|numeric|required");
                $this->form_validation->set_rules('okod', 'Öğrenci Kart Kod', $kural);
                if ($this->form_validation->run() == FALSE) {
                    $obilgi['ogrenci'] = $this->idare_model->obul($id);
                    if (isset($obilgi['ogrenci']['islem']) and $obilgi['ogrenci']['islem'] == 0) {
                        $uri = "idare/ogrenciler";
                        show_error("Öğrenci bulunamadı.&&$uri", "", "Hata!");
                    } else {
                        $oduz['icerik'] = $this->load->view("idare/oduz", $obilgi, TRUE);
                        $this->load->view("idare/template", $oduz);
                    }
                } else {
                    $adsoyad = $this->input->post("adsoyad", TRUE);
                    $no = $this->input->post("no", TRUE);
                    $okod = $this->input->post("okod", TRUE);
                    $bilgi['data'] = array(
                        "ad_soyad" => $adsoyad,
                        "no" => $no,
                        "okod" => $okod
                    );
                    $bilgi['id'] = $id;
                    $sonuc = $this->idare_model->oduz($bilgi);
                    if ($sonuc == 1) {
                        $url = "idare/ogrenciler";
                        show_error("Öğrenci başarıyla düzenlendi.&&$url", "", "Başarılı!");
                    } else {
                        show_error("Bir hata oluştu ve öğrenci düzenlenemedi. Lütfen tekrar deneyin.", "", "Hata!");
                    }
                }
            } else {
                $uri = "idare/ogrenciler";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    ## Kütüphane İşlemleri ##
    public function kao()
    {
        $kutuphane['kutuphane'] = $this->idare_model->kao();
        $kao['icerik'] = $this->load->view("idare/kao", $kutuphane, TRUE);
        $this->load->view("idare/template", $kao);
    }

    public function ggk()
    {
        $kutuphane['kutuphane'] = $this->idare_model->ggk();
        $ggk['icerik'] = $this->load->view("idare/ggk", $kutuphane, TRUE);
        $this->load->view("idare/template", $ggk);
    }

    ## Tür İşlemleri ##
    public function turler()
    {
        $turler['turler'] = $this->idare_model->turler();
        $data['icerik'] = $this->load->view("idare/turler", $turler, TRUE);
        $this->load->view("idare/template", $data);
    }

    public function tsil($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/turler");
        } else {
            if (is_numeric($id)) {
                $sil = $this->idare_model->tsil($id);
                if ($sil == 1) {
                    show_error("Tür başarıyla silindi.", "", "Başarılı!");
                } else {
                    show_error("Bir hata oluştu ve tür silinemedi. Lütfen tekrar deneyin.", "", "Hata!");
                }
            } else {
                $uri = "idare/turler";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    public function tduz($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/turler");
        } else {
            if (is_numeric($id)) {
                $this->form_validation->set_rules('ad', 'Tür Adı', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $tbilgi['tur'] = $this->idare_model->turler($id);
                    if (isset($tbilgi['tur']['kod']) and $tbilgi['tur']['kod'] == 0) {
                        $uri = "idare/turler";
                        show_error("Tür bulunamadı.&&$uri", "", "Hata!");
                    } else {
                        $tduz['icerik'] = $this->load->view("idare/tduz", $tbilgi, TRUE);
                        $this->load->view("idare/template", $tduz);
                    }
                } else {
                    $ad = $this->input->post("ad", TRUE);
                    $bilgiler = array(
                        "data" => array(
                            "tur_adi" => $ad
                        ),
                        "id" => $id
                    );
                    $duzenle = $this->idare_model->tduz($bilgiler);
                    if ($duzenle == 1) {
                        $uri = "idare/turler";
                        show_error("Tür başarıyla düzenlendi.&&$uri", "", "Başarılı!");
                    } else {
                        show_error("Bir hata oluştu ve tür düzenlenemedi. Lütfen tekrar deneyin.", "", "Hata!");
                    }
                }
            } else {
                $uri = "idare/turler";
                show_error("Girilen İD sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    public function tekle()
    {
        $this->form_validation->set_rules('ad', 'Tür Adı', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $tekle['icerik'] = $this->load->view("idare/tekle", "", TRUE);
            $this->load->view("idare/template", $tekle);
        } else {
            $ad = $this->input->post("ad");
            $bilgi = array(
                "tur_adi" => $ad
            );
            $turekle = $this->idare_model->tekle($bilgi);
            if ($turekle == 1) {
                show_error("Tür başarıyla eklendi.", "", "Başarılı!");
            } elseif ($turekle == 2) {
                show_error("Eklemeye çalıştığınız tür sistemde mevcut.", "", "Hata!");
            } else {
                show_error("Lütfen tekrar deneyin.", "", "Hata!");
            }
        }
    }

    public function ttekle()
    {
        set_time_limit(0);
        klasor_temizle('kutuphane');
        $this->form_validation->set_rules('sutun', 'Türlerin Olduğu Sütun', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $ttekle['icerik'] = $this->load->view("idare/ttekle", "", TRUE);
            $this->load->view("idare/template", $ttekle);
        } else {
            $sutun = strtoupper($this->input->post("sutun", TRUE));
            $config['upload_path'] = 'kutuphane/';
            $config['allowed_types'] = 'xls|xlsx|ods';
            $config['file_name'] = rand(0, 99999999);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('excel')) {
                show_error("Bir sorun oluştu ve dosya yüklenemedi.", "", "Hata!");
            } else {
                $this->load->library("phpexcel");
                $dosyadata = $this->upload->data();
                $dosya = "kutuphane/" . $dosyadata['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($dosya);
                $excel_satirlar = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $excel_satirlar = array_map('array_filter', $excel_satirlar);
                $excel_satirlar = array_filter($excel_satirlar);
                foreach ($excel_satirlar as $excel_satir) {
                    $bilgiler = array(
                        "tur_adi" => $excel_satir[$sutun]
                    );
                    $ekle = $this->idare_model->tekle($bilgiler);
                    if ($ekle == 1) {
                        $islem['hata'][] = array(
                            "sonuc" => "Tür başarılı bir şekilde eklendi.&&1<br>"
                        );
                    } elseif ($ekle == 2) {
                        $islem['hata'][] = array(
                            "sonuc" => "<b>" . $excel_satir[$sutun] . "</b> adında bir tür sisteme kayıtlı.&&2<br>",
                        );
                    } else {
                        $islem['hata'][] = array(
                            "sonuc" => "<b>" . $excel_satir[$sutun] . "</b> türü eklenirken bir hata oluştu ve tür eklenemedi. Lütfen tekrar deneyin.&&2<br>"
                        );
                    }
                }
                $this->load->view("errors/html/error_toplu", $islem);
            }
        }
    }

    ## Kitap İşlemleri ##
    public function ksil($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/kitaplar");
        } else {
            if (is_numeric($id)) {
                $sil = $this->idare_model->ksil($id);
                if ($sil == 1) {
                    show_error("Kitap başarıyla silindi.", "", "Başarılı!");
                } elseif ($sil == 2) {
                    show_error("Kitabı okuyan öğrenci var. İlk önce öğrencinin kitabı teslim etmesi gerekiyor.", "", "Hata!");
                } else {
                    show_error("Bir hata oluştu ve kitap silinemedi. Lütfen tekrar deneyin.", "", "Hata!");
                }
            } else {
                $uri = "idare/kitaplar";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    public function kitaplar()
    {
        $kitaplar['kitaplar'] = $this->idare_model->kitaplar();
        $data['icerik'] = $this->load->view("idare/kitaplar", $kitaplar, TRUE);
        $this->load->view("idare/template", $data);
    }

    public function kekle()
    {
        $kural = 'trim|required';
        $this->form_validation->set_rules('ad', 'Kitap Adı', $kural);
        $this->form_validation->set_rules('yazar', 'Kitap Yazarı', $kural);
        $this->form_validation->set_rules('isbn', 'ISBN', $kural . "|numeric");
        $this->form_validation->set_rules('tur', 'Kitap Tür', $kural . "|numeric");
        $this->form_validation->set_rules('tasnif', 'Kitap Tasnif No', $kural . "|numeric");
        $this->form_validation->set_rules('sira', 'Kitap Sıra No', $kural . "|numeric");
        $this->form_validation->set_rules('konu', 'Kitap Konusu', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $turler['turler'] = $this->idare_model->turler();
            $kekle['icerik'] = $this->load->view("idare/kekle", $turler, TRUE);
            $this->load->view("idare/template", $kekle);
        } else {
            $ad = $this->input->post("ad", TRUE);
            $yazar = $this->input->post("yazar", TRUE);
            $isbn = $this->input->post("isbn", TRUE);
            $tur = $this->input->post("tur", TRUE);
            $tasnif = $this->input->post("tasnif", TRUE);
            $sira = $this->input->post("sira", TRUE);
            $konu = $this->input->post("konu", TRUE);
            $data = array(
                "kitap_adi" => $ad,
                "kitap_yazar" => $yazar,
                "kitap_tur" => $tur,
                "isbn" => $isbn,
                "tasnif" => $tasnif,
                "sira" => $sira,
                "kitap_konu" => $konu
            );
            $kitape = $this->idare_model->kekle($data);
            if ($kitape == 1) {
                show_error("Kitap başarılı bir şekilde eklendi.", "", "Başarılı!");
            } else {
                show_error("Bir hata oluştu ve kitap eklenemedi. Lütfen tekrar deneyin.", "", "Hata!");
            }
        }
    }

    public function tkekle()
    {
        set_time_limit(0);
        klasor_temizle('kutuphane');
        $kural = 'trim|required';
        $this->form_validation->set_rules('sutun', 'Kitap Adı Sütunu', $kural);
        $this->form_validation->set_rules('sutun2', 'Kitap Yazarı Sütunu', $kural);
        $this->form_validation->set_rules('sutun3', 'ISBN Sütunu', $kural);
        $this->form_validation->set_rules('sutun4', 'Tasnif No Sütunu', $kural);
        $this->form_validation->set_rules('sutun5', 'Sıra No Sütunu', $kural);
        $this->form_validation->set_rules('sutun6', 'Konu Sütunu', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $turler['turler'] = $this->idare_model->turler();
            $tkekle['icerik'] = $this->load->view("idare/tkekle", $turler, TRUE);
            $this->load->view("idare/template", $tkekle);
        } else {
            $ad = strtoupper($this->input->post("sutun", TRUE));
            $yazar = strtoupper($this->input->post("sutun2", TRUE));
            $isbn = strtoupper($this->input->post("sutun3", TRUE));
            $tasnif = strtoupper($this->input->post("sutun4", TRUE));
            $sira = strtoupper($this->input->post("sutun5", TRUE));
            $konu = strtoupper($this->input->post("sutun6", TRUE));
            $tur = $this->input->post("tur", TRUE);
            $config['upload_path'] = 'kutuphane/';
            $config['allowed_types'] = 'xls|xlsx|ods';
            $config['file_name'] = rand(0, 99999999);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('excel')) {
                show_error("Bir sorun oluştu ve dosya yüklenemedi.", "", "Hata!");
            } else {
                $this->load->library("phpexcel");
                $dosyadata = $this->upload->data();
                $dosya = "kutuphane/" . $dosyadata['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($dosya);
                $excel_satirlar = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $excel_satirlar = array_map('array_filter', $excel_satirlar);
                $excel_satirlar = array_filter($excel_satirlar);
                foreach ($excel_satirlar as $excel_satir) {
                    $bilgiler = array(
                        "kitap_adi" => $excel_satir[$ad],
                        "kitap_yazar" => $excel_satir[$yazar],
                        "kitap_tur" => $tur,
                        "isbn" => $excel_satir[$isbn],
                        "tasnif" => $excel_satir[$tasnif],
                        "sira" => $excel_satir[$sira]
                    );
                    if(!empty($konu)){
                        $bilgiler['kitap_konu'] = $excel_satir[$konu];
                    }
                    $ekle = $this->idare_model->kekle($bilgiler);
                    if ($ekle == 1) {
                        $islem['hata'][] = array(
                            "sonuc" => "Kitap başarılı bir şekilde eklendi.&&1<br>"
                        );
                    } else {
                        $islem['hata'][] = array(
                            "sonuc" => "<b>" . $excel_satir[$isbn] . "</b> ISBN'e sahip kitap eklenirken bir hata oluştu ve kitap eklenemedi. Lütfen tekrar deneyin.&&2<br>"
                        );
                    }
                }
                $this->load->view("errors/html/error_toplu", $islem);
            }
        }
    }

    public function kduz($id)
    {
        if (empty($id)) {
            redirect(base_url() . "idare/kitaplar");
        } else {
            if (is_numeric($id)) {
                $kural = 'trim|required';
                $this->form_validation->set_rules('ad', 'Kitap Adı', $kural);
                $this->form_validation->set_rules('yazar', 'Kitap Yazarı', $kural);
                $this->form_validation->set_rules('isbn', 'ISBN', $kural . "|numeric");
                $this->form_validation->set_rules('tur', 'Kitap Tür', $kural . "|numeric");
                $this->form_validation->set_rules('tasnif', 'Kitap Tasnif No', $kural . "|numeric");
                $this->form_validation->set_rules('sira', 'Kitap Sıra No', $kural . "|numeric");
                $this->form_validation->set_rules('konu', 'Kitap Konusu', 'trim');
                if ($this->form_validation->run() == FALSE) {
                    $kbilgi['kitap'] = $this->idare_model->kbul($id);
                    $kbilgi['turler'] = $this->idare_model->turler();
                    if (isset($kbilgi['kitap']['islem']) and $kbilgi['kitap']['islem'] == 0) {
                        $uri = "idare/kitaplar";
                        show_error("Kitap bulunamadı.&&$uri", "", "Hata!");
                    } else {
                        $kduz['icerik'] = $this->load->view("idare/kduz", $kbilgi, TRUE);
                        $this->load->view("idare/template", $kduz);
                    }
                } else {
                    $ad = $this->input->post("ad", TRUE);
                    $yazar = $this->input->post("yazar", TRUE);
                    $isbn = $this->input->post("isbn", TRUE);
                    $tur = $this->input->post("tur", TRUE);
                    $tasnif = $this->input->post("tasnif", TRUE);
                    $sira = $this->input->post("sira", TRUE);
                    $konu = $this->input->post("konu", TRUE);
                    $bilgi['data'] = array(
                        "kitap_adi" => $ad,
                        "kitap_yazar" => $yazar,
                        "kitap_tur" => $tur,
                        "isbn" => $isbn,
                        "tasnif" => $tasnif,
                        "sira" => $sira,
                        "kitap_konu" => $konu
                    );
                    $bilgi['id'] = $id;
                    $sonuc = $this->idare_model->kduz($bilgi);
                    if ($sonuc == 1) {
                        $url = "idare/kitaplar";
                        show_error("Kitap başarıyla düzenlendi.&&$url", "", "Başarılı!");
                    } else {
                        show_error("Bir hata oluştu ve kitap düzenlenemedi. Lütfen tekrar deneyin.", "", "Hata!");
                    }
                }
            } else {
                $uri = "idare/kitaplar";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    ## Yıl Sonu Raporu ##
    public function yilsonu($yil=null){
        @$yil = $this->security->xss_clean($yil);
        if(isset($yil) and is_numeric($yil) and !empty($yil)){
            if (!isset($_SESSION['kullanici'])) {
                redirect(base_url() . "idare/giris", "location");
            }else{
                klasor_temizle('kutuphane');
                ooks($yil);
                ggk();
                tb();
                $this->load->library('zip');
                $this->zip->read_dir('kutuphane');
                klasor_temizle('kutuphane');
                $this->zip->download('Yıl Sonu Raporu.zip');    
            }
        }else{
            redirect(base_url()."idare");
        }
    }

    ## DEWEY Onlu Sınıflama Sistemi ##
    public function dewey(){
        $json = file_get_contents('theme/dewey.json');
        $islem['kutuphanecilik'] = json_decode($json,'true');
        $data['icerik'] = $this->load->view("idare/dewey", $islem, TRUE);
        $this->load->view("idare/template", $data);
    }

    ## İdare İşlemleri ##
    public function iekle(){
        $kural = 'trim|required';
        $this->form_validation->set_rules('kadi', 'Kullanıcı Adı', $kural);
        $this->form_validation->set_rules('eposta', 'E-Posta', $kural);
        $this->form_validation->set_rules('sifre', 'Şifre', $kural);
        if ($this->form_validation->run() == FALSE) {
            $iek['icerik'] = $this->load->view("idare/iekle", '', TRUE);
            $this->load->view("idare/template", $iek);
        } else {
            $kadi = $this->input->post("kadi", TRUE);
            $eposta = $this->input->post("eposta", TRUE);
            $sifre = $this->input->post("sifre", TRUE);
            $data = array(
                "kadi" => $kadi,
                "eposta" => $eposta,
                "sifre" => md5(sha1($sifre))
            );
            $iekle = $this->idare_model->iekle($data);
            if ($iekle == 1) {
                show_error("İdare hesabı başarılı bir şekilde eklendi.", "", "Başarılı!");
            } else {
                show_error("Bir hata oluştu ve idare hesabı eklenemedi. Lütfen tekrar deneyin.", "", "Hata!");
            }
        }
    }

    public function iduz($id){
        if (empty($id)) {
            redirect(base_url() . "idare/ihesaplari");
        } else {
            if (is_numeric($id)) {
                $kural = 'trim|required';
                $this->form_validation->set_rules('kadi', 'Kullanıcı Adı', $kural);
                $this->form_validation->set_rules('eposta', 'E-Posta', $kural);
                $this->form_validation->set_rules('sifre', 'Şifre', 'trim');
                if ($this->form_validation->run() == FALSE) {
                    $ibilgi['idare'] = $this->idare_model->ibul($id);
                    if (isset($ibilgi['idare']['islem']) and $ibilgi['idare']['islem'] == 0) {
                        $uri = "idare/ihesaplari";
                        show_error("İdare hesabı bulunamadı.&&$uri", "", "Hata!");
                    } else {
                        $iduz['icerik'] = $this->load->view("idare/iduz", $ibilgi, TRUE);
                        $this->load->view("idare/template", $iduz);
                    }
                } else {
                    $kadi = $this->input->post("kadi", TRUE);
                    $eposta = $this->input->post("eposta", TRUE);
                    $sifre = $this->input->post("sifre", TRUE);
                    if(empty($sifre)){
                        $bilgial = $this->idare_model->ibul($id);
                        $sifre = $bilgial['sifre'];
                    }else{
                        $sifre = md5(sha1($sifre));
                    }
                    $bilgiler = array(
                        "data" => array(
                            "kadi" => $kadi,
                            "eposta" => $eposta,
                            "sifre" => $sifre
                        ),
                        "id" => $id
                    );
                    $duzenle = $this->idare_model->iduz($bilgiler);
                    if ($duzenle == 1) {
                        $uri = "idare/ihesaplari";
                        show_error("İdare hesabı başarıyla düzenlendi.&&$uri", "", "Başarılı!");
                    } else {
                        show_error("Bir hata oluştu ve idare hesabı düzenlenemedi. Lütfen tekrar deneyin.", "", "Hata!");
                    }
                }
            } else {
                $uri = "idare/ihesaplari";
                show_error("Girilen İD sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }

    public function ihesaplari(){
        $ihesaplar['hesaplar'] = $this->idare_model->ihesaplar();
        $data['icerik'] = $this->load->view("idare/ihesaplari", $ihesaplar, TRUE);
        $this->load->view("idare/template", $data);
    }

    public function isil($id){
        if (empty($id)) {
            redirect(base_url() . "idare/ihesaplari");
        } else {
            if (is_numeric($id)) {
                $sil = $this->idare_model->isil($id);
                if ($sil == 1) {
                    show_error("İdare hesabı başarıyla silindi.", "", "Başarılı!");
                }else {
                    show_error("Bir hata oluştu ve idare hesabı silinemedi. Lütfen tekrar deneyin.", "", "Hata!");
                }
            } else {
                $uri = "idare/ihesaplari";
                show_error("Girilen ID sayısal değere sahip değil.&&$uri", "", "Hata!");
            }
        }
    }
}