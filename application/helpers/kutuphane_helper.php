<?php

function okunma_sayisi($id){
    $CI =& get_instance();
    $id = $CI->security->xss_clean($id);
    return $CI->db->where("kitap_id", $id)->count_all_results("kutuphane");
}

function ooks($yil=null){
    if(empty($yil) or !isset($yil) or !is_numeric($yil)){
        $yil = date("Y");
    }
    $CI =& get_instance();
    $yil = $CI->security->xss_clean($yil);
    $time = time();

    ## Veritabanı İşlemi ##
    $date = "'$yil%'";
    $bul = $CI->db->query("SELECT ogrenciler.no,ogrenciler.ad_soyad, COUNT(kutuphane.ogrenci_id) as sayi FROM kutuphane INNER JOIN ogrenciler ON ogrenciler.ogrenciler_id = kutuphane.ogrenci_id 
WHERE kutuphane.teslim_tarihi LIKE($date) and kutuphane.teslim_edildimi = 1 GROUP BY kutuphane.ogrenci_id");
    if ($CI->db->affected_rows() >= 1) {
        $data = $bul->result_array();
        $dizi = array(
            'no' => "Okul No",
            'ad_soyad' => "Öğrenci Ad Soyad",
            "sayi" => "Okuduğu Toplam Kitap Sayısı"
        );
        array_unshift($data,$dizi);
            
        ## Excel İşlemleri
        $CI->load->library("phpexcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->fromArray($data, null, "A1");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("kutuphane/$yil-Ogrencilerin Okudugu Kitap Sayilari-$time.xlsx");
        return "1";
    }else{
        return "0";
    }
}

function ggk(){
    $CI =& get_instance();
    $time = time();
    $where = array(
        "DATEDIFF(NOW(),kutuphane.tahmini_teslim_tarihi) >=" => 1,
        "teslim_edildimi" => 0
    );
    $select = array('alim_tarihi','tahmini_teslim_tarihi','ad_soyad','no','kitap_adi','isbn');
    $kutuphane = $CI->db->join("ogrenciler", "kutuphane.ogrenci_id = ogrenciler.ogrenciler_id", "inner")->join("kitaplar", "kutuphane.kitap_id = kitaplar.kitaplar_id")->where($where)->select($select)->get("kutuphane");
    $data = $kutuphane->result_array();
    if(!empty($data)){    
        ## Data Düzenleme ##
        $dizi = array(
            'alim_tarihi' => "Alım Tarihi",
            'tahmini_teslim_tarihi' => "Tahmini Teslim Tarihi",
            'ad_soyad' => "Ad Soyad",
            'no' => 'Okul No',
            'kitap_adi' => 'Kitap Adı',
            'isbn' => "ISBN"
        );
        array_unshift($data,$dizi);
        ## Excel İşlemleri
        $CI->load->library("phpexcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->fromArray($data, null, "A1");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("kutuphane/Geri Getirilmeyen Kitaplar-$time.xlsx");
        return "1";
    }else{
        return "0";
    }
}

function tb(){
    $CI =& get_instance();
    $time = time();
    $CI->load->model('idare_model');
    $CI->load->model('main_model');
    $data[0] = array(
        'tos' => 'Toplam Öğrenci Sayısı',
        'tks' => 'Toplam Kitap Sayısı',
        'oks' => 'Öğrencide Olan Kitap Sayısı',
        'gks' => 'Getirilmeyen Kitap Sayısı',
        'eco' => 'En Çok Kitap Okuyan Öğrenci',
        'eck' => 'En Çok Okunan Kitap'
    );
    $tos = $CI->idare_model->tos();
    $tks = $CI->idare_model->tks();
    $oks = $CI->idare_model->oks();
    $gks = $CI->idare_model->gks();
    $eco = $CI->main_model->eco();
    $eck = $CI->main_model->eck();
    $data[1] = array(
        'tos' => $tos['COUNT(*)'],
        'tks' => $tks['COUNT(*)'],
        'oks' => $oks['COUNT(*)'],
        'gks' => $gks['COUNT(*)'],
        'eco' => $eco['no']."-".$eco['ad_soyad'],
        'eck' => $eck['kitap_adi']
    );
    $CI->load->library("phpexcel");
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->fromArray($data, null, "A1");
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save("kutuphane/Temel Bilgiler-$time.xlsx");
}

function klasor_temizle($klasor)
{
    $oku = opendir($klasor);
    while ($sonuc = readdir($oku)) {
        $sonuck = explode(".", $sonuc);
        $sonuck = end($sonuck);
        if ($sonuck == "png" || $sonuck == "ods" || $sonuck == "xls" || $sonuck == "xlsx" || $sonuck == "zip") {
            unlink("$klasor/" . $sonuc);
        }
    }
}

?>